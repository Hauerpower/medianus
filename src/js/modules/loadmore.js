export default function loadMoreInit(){
	(function($){
		
		function ajax_show_more($wrapper, template, btn){
		
	    if( btn.hasClass('is-loading') ) return;
	
	
	    var data = {
	    	'action': 'loadmore',
        'query': wp_params.posts, // that's how we get wp_params from wp_localize_script() function
        'page' : wp_params.current_page,
        'template' : template
	    }; 
	
	    $.ajax({
	        url : wp_params.ajax_url, // AJAX handler
	        data : data,
	        type : 'POST',
	        beforeSend : function ( ) {
		        btn.addClass('is-loading');
	        },
	        success : function( data ){
	          if( data ) { 

              var disp = $wrapper.children().first().css('display');

              $wrapper.append(data);
              $wrapper.find('.ajax-loaded').css({display: disp}).hide();
              $wrapper.find('.ajax-loaded').fadeIn(400).removeClass('ajax-loaded');

              btn.removeClass('is-loading');

              wp_params.current_page++;

              if ( wp_params.current_page == wp_params.max_page ) {
                  btn.addClass('fading').fadeOut(400);
                  btn.removeClass('is-loading').addClass('no-posts');
              }
              // you can also fire the "post-load" event here if you use a plugin that requires it
              $( document.body ).trigger( 'post-load' );
	
	          } else {
              btn.addClass('fading').fadeOut(400);
              btn.removeClass('is-loading').addClass('no-posts');
	          }
	        }
	    });
		}
		
		
		// Preload icon
		$('[data-loadmore]').addClass('is-loading');
		setTimeout(function(){
			$('[data-loadmore]').removeClass('is-loading')
		}, 20);
		
    $('[data-loadmore]').on('click', function(){
      var $wrapper = $( $(this).attr('data-wrap') );
      var btn = $(this);
      var template = $(this).attr('data-template');
      
      ajax_show_more($wrapper, template, btn);
    });
		
	})(jQuery);
}